From gpfsl.base_logic Require Import na meta_data.
From gpfsl.logic Require Import proofmode.

Require Import iris.prelude.options.

Definition new : val :=
  λ: ["n"],
    if: "n" ≤ #0 then #((42%positive, 1337):loc)
    else Alloc "n".

Definition delete : val :=
  λ: ["n"; "loc"],
    if: "n" ≤ #0 then #☠
    else Free "n" "loc".

Section specs.
Context `{!noprolG Σ}.

Lemma wp_new E tid (n : Z):
  ↑histN ⊆ E → (0 ≤ n)%Z →
  {{{ True }}} new [ #n ] @ tid; E
  {{{ l, RET LitV $ LitLoc l;
      (⎡†l…(Z.to_nat n)⎤ ∨ ⌜n = 0⌝) ∗
      l ↦∗ repeat #☠ (Z.to_nat n) ∗
      [∗ list] i ∈ seq 0 (Z.to_nat n), meta_token (l >> i) ⊤ }}}.
Proof.
  iIntros (?? Φ) "_ HΦ". wp_lam. wp_op; case_bool_decide.
  - wp_if. assert (n = 0) as -> by lia. iApply "HΦ".
    rewrite own_loc_na_vec_nil. auto.
  - wp_if. wp_alloc l as "Hm" "H↦" "H†"; first lia. iApply "HΦ". subst. iFrame.
Qed.

Lemma wp_delete E tid (n:Z) l vl :
  ↑histN ⊆ E → n = length vl →
  {{{ l ↦∗ vl ∗ (⎡†l…(length vl)⎤ ∨ ⌜n = 0⌝) }}}
    delete [ #n; #l] @ tid; E
  {{{ RET #☠; True }}}.
Proof.
  iIntros (?? Φ) "[H↦ [H†|%]] HΦ";
    wp_lam; wp_op; case_bool_decide; try lia;
    wp_if; try wp_free; by iApply "HΦ".
Qed.
End specs.
